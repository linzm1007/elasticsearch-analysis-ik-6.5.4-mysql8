-- esciku.extended_word definition

CREATE TABLE `extended_word`
(
    `id`      int(11) NOT NULL AUTO_INCREMENT,
    `keyword` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
    `flag`    int(11) DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO esciku.extended_word
    (id, keyword, flag)
VALUES (1, '我是中国', 0);

-- esciku.stop_word definition

CREATE TABLE `stop_word`
(
    `id`      int(11) NOT NULL AUTO_INCREMENT,
    `keyword` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
    `flag`    int(11) DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO esciku.stop_word
    (id, keyword, flag)
VALUES (1, '国人', 0);
INSERT INTO esciku.stop_word
    (id, keyword, flag)
VALUES (2, '中国人', 0);
